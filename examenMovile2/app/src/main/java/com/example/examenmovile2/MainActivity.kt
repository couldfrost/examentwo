package com.example.examenmovile2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var liveDataString: LiviDatabool
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        liveDataString = LiviDatabool()
        liveDataString.cadena.observe(this, Observer(::updateUi))
         button.setOnClickListener{
             if(name.toString()==" " && lastname.toString()==" " && email.toString()==" ")
             {
                 liveDataString.cambiarMensaje(false)
             }
             else
             {
                 liveDataString.cambiarMensaje(true)             }

        }

        }
         private fun updateUi(boolean: Boolean){
            if(boolean)
            {
                Toast.makeText( this , "error" , Toast. LENGTH_LONG ).show()
            }
            else
            {
                Toast.makeText( this , "exito" , Toast. LENGTH_LONG ).show()
            }
    }
    class LiviDatabool: ViewModel(){
        val cadena: LiveData<Boolean>
        get()=_cadena
        private val _cadena= MutableLiveData<Boolean>()
        fun cambiarMensaje(boolean: Boolean){
            _cadena.value =boolean
        }
    }

}
